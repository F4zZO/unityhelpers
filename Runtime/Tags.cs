using System.Collections.Generic;
using UnityEngine;

public class Tags : MonoBehaviour
{
    [SerializeField] private List<Tag> tags;

    public List<Tag> All => this.tags;

    public bool HasTag(Tag t)
    {
        return this.tags.Contains(t);
    }

    public bool HasTag(string tagName)
    {
        return this.tags.Exists(t => t.Name == tagName);
    }

    public void AddTag(Tag t)
    {
        this.tags.Add(t);
    }
}
