using UnityEngine;

[CreateAssetMenu(fileName = "New Tag", menuName = "Tags/New Tag")]
public class Tag : ScriptableObject
{
    public string Name => this.name;

    public static new Tag CreateInstance(string tagName)
    {
        Tag tag = ScriptableObject.CreateInstance<Tag>();
        tag.name = tagName;
        return tag;
    }
}
